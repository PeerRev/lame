﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PeerReview.Startup))]
namespace PeerReview
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PeerReview.Models;

namespace PeerReview.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MyModel mm = new MyModel();
            CourseViewModel cvm = new CourseViewModel();
            foreach (var c in mm.CourseDB)
            {
                cvm.CourseView.Add(c);
            }
            return View(cvm);
        }

        public ActionResult CreateCourse()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCourse (Course model)
        {
            if (ModelState.IsValid)
            {
                using(var mm = new MyModel())
                {
                    mm.CourseDB.Add(model);
                    mm.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult NewCourse ()
        {
            return View();
        }
    }
}
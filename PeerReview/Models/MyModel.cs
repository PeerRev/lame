﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PeerReview.Models
{
    public class MyModel : DbContext
    {
        public MyModel()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MyModel>());
        }

        public virtual DbSet<Course> CourseDB { get; set; }
        public virtual DbSet<Homework> HomeworkDB { get; set; }
    }
}
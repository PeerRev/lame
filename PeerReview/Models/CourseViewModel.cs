﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeerReview.Models
{
    public class CourseViewModel
    {
        public List<Course> CourseView { get; set; }
        public CourseViewModel()
        {
            CourseView = new List<Course>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PeerReview.Models
{
    public class Homework
    {
        [Key]
        public int Id { get; set; }    
        public string InitialDate { get; set; } 
        public string Description { get; set; }
        public string DueDate { get; set; }
    }
}